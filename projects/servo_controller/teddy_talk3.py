import pyaudio
import numpy as np
from adafruit_servokit import ServoKit

# Setup for servo
kit = ServoKit(channels=16)
servo_channel = 0  # Change to your servo channel
kit.servo[servo_channel].angle = 90  # Initial position

# Setup for audio
CHUNK = 1024  # Buffer size
RATE = 44100  # Sampling rate

p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16,
                channels=1,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

def get_frequency(data):
    # Convert audio data to numpy array
    audio_data = np.frombuffer(data, dtype=np.int16)
    
    # Perform FFT on audio data
    fft_result = np.fft.fft(audio_data)
    frequencies = np.fft.fftfreq(len(fft_result))
    #print("Raw Freq: " + str(frequencies))
    
    # Find the peak frequency
    peak_index = np.argmax(np.abs(fft_result))
    peak_frequency = abs(frequencies[peak_index] * RATE)
    print("Peak: " + str(round(peak_frequency)))
    
    return peak_frequency

try:
    while True:
        data = stream.read(CHUNK)
        frequency = get_frequency(data)

        # Map frequency to servo angle (e.g., 50Hz to 500Hz -> angle 0 to 180)
        servo_angle = np.interp(frequency, [50, 500], [0, 180])
        
        # Ensure servo angle is within bounds
        servo_angle = max(0, min(180, servo_angle))
        
        # Set servo to the calculated angle
        kit.servo[servo_channel].angle = servo_angle
        print(f"Frequency: {frequency:.2f} Hz, Servo Angle: {servo_angle:.1f}")

except KeyboardInterrupt:
    print("Program stopped by user.")
finally:
    stream.stop_stream()
    stream.close()
    p.terminate()
