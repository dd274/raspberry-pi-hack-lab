import pyaudio

def list_audio_devices():
    """List all available audio devices"""
    p = pyaudio.PyAudio()
    info = "\nAvailable Audio Devices:\n"
    for i in range(p.get_device_count()):
        dev = p.get_device_info_by_index(i)
        info += f'Device {i}: {dev["name"]}\n'
        info += f'  Input Channels: {dev["maxInputChannels"]}\n'
        info += f'  Output Channels: {dev["maxOutputChannels"]}\n'
        info += f'  Default Sample Rate: {dev["defaultSampleRate"]}\n'
    p.terminate()
    return info

print(list_audio_devices())