import openai
import pyttsx3
import os


# Initialize OpenAI and text-to-speech engine
openai.api_key = os.environ.get('OPENAI_API')  # Replace with your actual API key
engine = pyttsx3.init()

def get_ai_response(prompt):
    try:
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "user", "content": prompt}
            ]
        )
        return response.choices[0].message.content
    except Exception as e:
        return f"An error occurred: {str(e)}"

def speak_text(text):
    engine.say(text)
    engine.runAndWait()

def main():
    print("AI Chatbot with Voice (Type 'quit' to exit)")
    
    while True:
        user_input = input("\nYou: ")
        
        if user_input.lower() == 'quit':
            print("Goodbye!")
            speak_text("Goodbye!")
            break
        
        # Get AI response
        ai_response = get_ai_response(user_input)
        
        # Print and speak the response
        print(f"\nAI: {ai_response}")
        speak_text(ai_response)

if __name__ == "__main__":
    main()