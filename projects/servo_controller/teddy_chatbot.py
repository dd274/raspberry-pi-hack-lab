import openai
from gtts import gTTS
import os
from playsound import playsound

# Set up your OpenAI API key
openai.api_key = os.environ.get('OPENAI_API')

def ask_openai(question):
    # Send the question to OpenAI's API and get the response
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",  # Replace with the model you prefer
        messages=[{"role": "user", "content": question}]
    )
    return response['choices'][0]['message']['content']

def speak_response(response_text):
    # Convert text to speech using gTTS
    tts = gTTS(response_text)
    audio_file = "response.mp3"
    tts.save(audio_file)
    playsound(audio_file)
    os.remove(audio_file)

# Main chatbot loop
while True:
    question = input("You: ")
    if question.lower() in ["exit", "quit", "stop"]:
        print("Chatbot: Goodbye!")
        break
    response = ask_openai(question)
    print("Chatbot:", response)
    speak_response(response)
