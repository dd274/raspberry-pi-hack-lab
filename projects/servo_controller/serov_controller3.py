from gpiozero import AngularServo, Device, Servo
from gpiozero.pins.pigpio import PiGPIOFactory

Device.pin_factory = PiGPIOFactory('127.0.0.1')

from time import sleep

#servo = Servo(21)
#mouth = AngularServo(21, min_angle=-90, max_angle=360)
mouth = AngularServo(16, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
eyes = AngularServo(20, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
nose = AngularServo(21, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
#mouth = Servo(21)
#eyes = Servo(20)

while True:
    #servo.min()
    mouth.angle = 0
    eyes.angle = 0
    nose.angle = 0
    #mouth.min()
    print('0')
    #sleep(2)
    #servo.mid()
    sleep(2)
    #servo.max()
    #mouth.max()
    mouth.angle = 180
    eyes.angle = 180
    nose.angle = 180
    print('180')
    sleep(2)