import pyaudio
import numpy as np
from scipy.fftpack import fft
from adafruit_servokit import ServoKit

# Initialize the servo kit for controlling the servo
kit = ServoKit(channels=16)
servo_channel = 0  # Adjust according to your setup

# Parameters for audio capture
CHUNK = 1024        # Number of audio samples per frame
FORMAT = pyaudio.paInt16  # Format of audio samples
CHANNELS = 1        # Mono audio
RATE = 44100        # Sample rate (Hz)

# Initialize PyAudio
p = pyaudio.PyAudio()

# Open audio stream
stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

def map_freq_to_angle(frequency, min_freq=20, max_freq=2000, min_angle=0, max_angle=180):
    # Map frequency to servo angle
    angle = (frequency - min_freq) * (max_angle - min_angle) / (max_freq - min_freq) + min_angle
    return max(min(angle, max_angle), min_angle)

print("Listening for audio frequencies...")

try:
    while True:
        # Read audio data
        data = np.frombuffer(stream.read(CHUNK), dtype=np.int16)
        
        # Perform FFT to get frequencies
        fft_data = fft(data)
        freqs = np.fft.fftfreq(len(fft_data))
        peak_freq = abs(freqs[np.argmax(np.abs(fft_data))] * RATE)
        
        # Map frequency to servo angle and set servo position
        servo_angle = map_freq_to_angle(peak_freq)
        kit.servo[servo_channel].angle = servo_angle
        print(f"Frequency: {peak_freq:.2f} Hz, Servo Angle: {servo_angle:.2f}")

except KeyboardInterrupt:
    print("Stopped by user")

finally:
    # Clean up
    stream.stop_stream()
    stream.close()
    p.terminate()
