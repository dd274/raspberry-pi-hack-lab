from gpiozero import AngularServo, Device, Servo
from gpiozero.pins.pigpio import PiGPIOFactory

Device.pin_factory = PiGPIOFactory('127.0.0.1')

from time import sleep
import pyaudio
import numpy as np
from multiprocessing import Process


#SET FOR SERVO
mouth = AngularServo(16, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
eyes = AngularServo(20, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
nose = AngularServo(21, min_angle=0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)

mouth.angle = 0
eyes.angle = 0
nose.angle = 0
#kit.servo[mouth].angle = 180  # Initial position
#kit.servo[eyes].angle = 0  # Initial position
#kit.servo[nose].angle = 0  # Initial position
#kit.servo[eyes].set_pulse_width_range(0,0)
#kit.servo[nose].set_pulse_width_range(0,0)
#kit.servo[nose].set_pulse_width_range(750, 2250)

# SETUP FOR AUDIO
CHUNK = 1024
RATE = 44100
FORMAT = pyaudio.paInt16
CHANNELS = 1

p = pyaudio.PyAudio()
stream = p.open(
     format=FORMAT,
     channels=CHANNELS,
     rate=RATE,
     input=True,
     frames_per_buffer=CHUNK
)

def blink_subprocess():
    # FUNCTION TO CONTROL THE BLINK PROCESS
    while True:
        print('blink')
        #kit.servo[eyes].set_pulse_width_range(750, 2250)
        sleep(0.5)
        eyes.angle = 180
        sleep(0.5)
        eyes.angle = 0
        #kit.servo[eyes].set_pulse_width_range(0,0)
        sleep(9)

def get_frequency(data):
     # Convert audio to numpy array
     audio_data = np.frombuffer(data, dtype=np.int16)

     # Perform FFT on audio array
     fft_result = np.fft.fft(audio_data)
     frequencies = np.fft.fftfreq(len(fft_result))

     # Find peak frequency
     peak_index = np.argmax(np.abs(fft_result))
     peak_frequency = abs(frequencies[peak_index]*RATE)

     return(peak_frequency)
#i2c = busio.I2C(board.SCL, board.SDA)
#pwm = adafruit_pca9685.PCA9685(i2c)

# kit.servo[0].angle = 180
# sleep(1)
# kit.servo[0].angle = 90
# sleep(1)
blink = Process(target=blink_subprocess)
blink.start()
try:
    while True:
        data = stream.read(CHUNK)
        frequency = get_frequency(data)

        # MAP FREQUENCEY TO SERVO ANGLE (50hz to 500hz = ANGLE 0 to 180)
        servo_angle = np.interp(frequency, [50, 500], [0, 180])

        # ENSURE ANGLE IS WITHIN BOUNDS
        servo_angle = max(0, min(180, servo_angle))

        #SET SERVO ANGLE
        #kit.servo[mouth].angle = servo_angle#180
        nose.angle = servo_angle
        mouth.angle = servo_angle
    #     print('Thum: Angle 100')
        #sleep(1)
        #kit.servo[0].angle = 90
        print(f"Frequency: {frequency:.1f} Hz, Servo Angle: {servo_angle:.1f}")
except KeyboardInterrupt:
    print("Program stopped by user.")
finally:
    stream.stop_stream()
    stream.close()
    p.terminate()
    blink.terminate()
