import numpy as np
import pyaudio
import struct
import RPi.GPIO as GPIO

# Motor setup
GPIO.setmode(GPIO.BCM)
motor1_pins = [18, 23, 24, 25]  # Replace with your motor pins
for pin in motor1_pins:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)

# Audio setup
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100

p = pyaudio.PyAudio()
stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

def get_dominant_frequency():
    data = stream.read(CHUNK)
    data_int = np.frombuffer(data, dtype=np.int16)
    fft = np.fft.fft(data_int)
    freqs = np.fft.fftfreq(len(fft))
    idx = np.argmax(np.abs(fft))
    freq = freqs[idx] * RATE
    return abs(freq)

def control_motor(freq):
    if freq > 500:
        # Rotate motor clockwise
        GPIO.output(motor1_pins[0], 1)
        GPIO.output(motor1_pins[1], 0)
        GPIO.output(motor1_pins[2], 1)
        GPIO.output(motor1_pins[3], 0)
    elif freq > 200:
        # Rotate motor counter-clockwise
        GPIO.output(motor1_pins[0], 0)
        GPIO.output(motor1_pins[1], 1)
        GPIO.output(motor1_pins[2], 0)
        GPIO.output(motor1_pins[3], 1)
    else:
        # Stop motor
        GPIO.output(motor1_pins[0], 0)
        GPIO.output(motor1_pins[1], 0)
        GPIO.output(motor1_pins[2], 0)
        GPIO.output(motor1_pins[3], 0)

try:
    while True:
        freq = get_dominant_frequency()
        print("Frequency:", freq)
        control_motor(freq)

except KeyboardInterrupt:
    GPIO.cleanup()
    stream.stop_stream()
    stream.close()
    p.terminate()