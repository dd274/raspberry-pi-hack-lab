from adafruit_servokit import ServoKit
from time import sleep
from gtts import gTTS
import os
import pyaudio
import numpy as np

# Audio settings
CHUNK = 1024
RATE = 44100

#Inititalize Pyaudio
p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=1, rate=RATE, input=True, frames_per_buffer=CHUNK)

kit = ServoKit(channels=16)

eyes_open = 0
eyes_closed = 180

#mouth = kit.servo[0].angle
eyes = kit.servo[1].angle
nose = kit.servo[2].angle


def blink():
    print('blinking')
    kit.servo[1].angle = eyes_open
    sleep(0.5)
    kit.servo[1].angle = eyes_closed
    sleep(0.5)
    kit.servo[1].angle = eyes_open

def analyze_frequency():
    # Analyze Stream
    #data = np.frombuffer(stream.read(CHUNK, exception_on_overflow=False), dtype=np.int16)
    #fft = np.fft.fft(data)
    #freqs = np.fft.fftfreq(len(fft))
    data = stream.read(CHUNK)
    data_int = np.frombuffer(data, dtype=np.int16)
    fft = np.fft.fft(data_int)
    freqs = np.fft.fftfreq(len(fft))

    # get dominant frequency
    dominant_freq = abs(freqs[np.argmax(np.abs(fft[1:])) + 1])
    return dominant_freq

def control_servo_based_on_frequency(frequency):
    angle = min(max(int(frequency/10), 0), 180)
    kit.servo[1].angle = angle

def speak_text(text):
    tts = gTTS(text=text, lang='en')
    tts.save('speech.mp3')
    #os.system('mpg321 speech.mp3')
    #os.remove('speech.mp3')

try:
    # Example use
    speak_text("Hello, I am your robot assistant.")
    while True:
        frequency = analyze_frequency()
        control_servo_based_on_frequency(frequency)
except KeyboardInterrupt:
    pass
finally:
    stream.stop_stream()
    stream.close()
    p.terminate()



#while True:
    #pwm.set_pwm(0,'on')
    #kit.servo[0]._pwm_out.duty_cycle = 1
    #kit.servo[0].angle = 0
    #print('Thum: Angle 100')
    #sleep(2)
    #kit.servo[0].angle = 180
    #print('Thumb: Angle 180')
    #kit.servo[0]._pwm_out.duty_cycle = 0
    #pwm.set_pwm(0,'off')
    #sleep(0.5)
    #blink()
    #sleep(5)
