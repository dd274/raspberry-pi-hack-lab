# sudo apt-get instlal mpg123
import openai
import os
from gtts import gTTS
import subprocess

openai.api_key = os.environ.get('OPENAI_API')  # Replace with your actual API key
engine = pyttsx3.init()

def get_ai_response(prompt):
    try:
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "user", "content": prompt}
            ]
        )
        return response.choices[0].message.content
    except Exception as e:
        return f"An error occurred: {str(e)}"

def speak_text(text):
    tts = gTTS(text=text, lang='en')
    tts.save('response.mp3')
    # Play the audio file using mpg123
    try:
        subprocess.run(['mpg123', '-q', 'response.mp3'])  # -q flag makes it quiet (no output)
    except Exception as e:
        print(f"Error playing sound: {e}")
    
    # Remove temporary file
    try:
        os.remove("response.mp3")
    except:
        pass
    #playsound('speech2.mp3')
    #os.remove('speech2.mp3')
    #engine.say(text)
    #engine.runAndWait()

def main():
    print("AI Chatbot with Voice (Type 'quit' to exit)")
    
    while True:
        user_input = input("\nYou: ")
        
        if user_input.lower() == 'quit':
            print("Goodbye!")
            speak_text("Goodbye!")
            break
        
        # Get AI response
        ai_response = get_ai_response(user_input)
        
        # Print and speak the response
        print(f"\nAI: {ai_response}")
        speak_text(ai_response)

if __name__ == "__main__":
    main()