import pi_servo_hat
import time

test = pi_servo_hat.PiServoHat()
test.restart()
# Moves servo position to 0 degrees (1ms), Channel 0
test.move_servo_position(0, 180)

# Pause 1 sec
time.sleep(1)

# Moves servo position to 90 degrees (2ms), Channel 0
test.move_servo_position(0, 90)

# Pause 1 sec
time.sleep(1)
# Moves servo position to 0 degrees (1ms), Channel 0
test.move_servo_position(2, 0)

# Pause 1 sec
time.sleep(1)

# Moves servo position to 90 degrees (2ms), Channel 0
test.move_servo_position(2, 90)

# Pause 1 sec
time.sleep(1)

# Pause 1 sec
time.sleep(1)
# Moves servo position to 0 degrees (1ms), Channel 0
test.move_servo_position(3, 0)

# Pause 1 sec
time.sleep(1)

# Moves servo position to 90 degrees (2ms), Channel 0
test.move_servo_position(3, 90)

# Pause 1 sec
time.sleep(1)
