'''
    This code is part of the Raspberry Pi Hack Lab
    Series. In this lab exercise, the primary focus
    is sensors. This is the basice code for a DHT11
    temperature sensor.

    We will also be using the OpenWeatherMap API
    to get current weather information. You can
    sign up for an account here:
    https://home.openweathermap.org/users/sign_up

    Libraries to install:
    pip install adafruit-circuitpython-dht
    pip install lcd-i2c
    
    More details:
    https://duke.is/pi-hack-lab3

'''

# IMPORT REQUIRED LIBRARIES
import requests
from lcd_i2c import LCD_I2C
import adafruit_dht
import board
import time

# SET UP VARIABLES
api_key = # Your API key
zip_code = '27701'
country_code = 'us'

# CREATE A VARIABLE FOR THE DHT11 SENSOR
sensor = adafruit_dht.DHT11(board.D4)

# CREATE A VARIABLE FOR THE LCD SCREEN
lcd = LCD_I2C(39, 16, 2)

# TURN ON THE BACKLIGHT AND CURSOR FOR THE LCD SCREEN
lcd.backlight.on()
lcd.blink.on()

# CREATE AN API CALL AND STORE IT AS A VARIABLE
url = f'http://api.openweathermap.org/data/2.5/forecast?zip={zip_code},{country_code}&appid={api_key}'


# CREATE AN ENDLESS LOOP THAT GRABS THE TEMP AND HUMIDITY
while True:
    # STORE THE RESULTS OF THE API CALL AS A VARIABLE
    response = requests.get(url)
    # CAPTURE THE SENSOR TEMP (DEFAULTS TO CELCIUS)
    temp_c = sensor.temperature
    # CAPTURE THE SENSOR HUMIDITY
    humidity = sensor.humidity
    # CONVERT THE TEMP TO FAHRENHEIT
    temp_f = round(temp_c * (9 / 5) + 32)

    # IF THE API REQUEST IS SUCCESSFUL, RUN THE FOLLOWING CODE
    if response.status_code == 200:
        # CONVERT THE API RESULTS TO JSON
        data = response.json()
        # GRAB THE TEMPERATURE FROM THE JSON FILE
        api_temp_c = data['list'][0]['main']['temp']
        # CONVERT THE API TEMP TO FAHRENHEIT
        api_temp_f = round((api_temp_c * (9/5)) - 459.67)
        # PRINT RESULTS TO CONSOLE
        print(f'\nOutside Temp: {api_temp_f}F\nIndoor Temp: {temp_f}F\n____________________')
        # WRITE THE RESULTS TO THE LCD SCREEN
        lcd.cursor.setPos(0,0)
        lcd.write_text(f'OUTSIDE  INSIDE')
        lcd.cursor.setPos(1,0)
        lcd.write_text(f'  {api_temp_f} F     {temp_f} F  ')
    else:
        # IF THE API REQUEST IS UNSUCCESSFUL, PRINT AN ERROR
        print('Error fetching weather data')

    # WAIT 60 SECONDS TO ALLOW SPACE BETWEEN READINGS
    # THIS PREVENTS OVERLOAD ON THE API SERVER
    time.sleep(60)