## Hack Lab - Week 3 🌡️⛅🤏

### 🎉 I'm 'sensing' this is going to be an awesome Raspberry Pi project! That's right, we're going to be working with sensors this week! Get ready to embark on a sensor-filled adventure with "Make It Feel," the latest installment in our Raspberry Pi Hack Labs series! This lesson is your golden ticket to transforming your Raspberry Pi into a sensory powerhouse, capable of detecting the world around it in ways you never imagined. Whether you're a hobbyist, educator, or just curious about the world of electronics, this lab will spark your creativity and elevate your Raspberry Pi projects to new heights. Join us and let's make your Raspberry Pi feel the world!

<p align="center"><img src="images/pir_test.gif"/></p>

## Week 3 Projects

In this repository, you'll find all the files for the projects intended to be done in week 3 including:

1. 🌡️ Using a DHT11 Sensor
2. ⛅ Creating A Basic Weather Station
3. 🏃 Using a PIR Motion Sensor
4. 🚨 Creating A Proximity Alarm With E-mail Alerts

## Wiring Diagrams

### DHT11 Sensor Diagram

This wiring diagram for the DHT11 temperature sensor

<p align="center"><img src="images/humidity_sensor2_bb.jpg" style="width:700px;"/></p>

### PIR Motion Sensor Diagram

This wiring diagram is for connecting a PIR motion sensor

<p align="center"><img src="images/pir_sensor_bb.jpg" style="width:700px;"/></p>
