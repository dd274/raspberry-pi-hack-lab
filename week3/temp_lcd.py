'''
    This code is part of the Raspberry Pi Hack Lab
    Series. In this lab exercise, the primary focus
    is sensors. This project uses a DHT11 sensor
    and an LCD to make a simple weather station.

    Libraries to install:
    pip install adafruit-circuitpython-dht
    pip install lcd-i2c
    
    More details:
    https://duke.is/pi-hack-lab3

'''

# IMPORT REQUIRED LIBRARIES
from lcd_i2c import LCD_I2C
import adafruit_dht
import board
import time

# CREATE A VARIABLE FOR THE DHT11 SENSOR
sensor = adafruit_dht.DHT11(board.D4)

# CREATE A VARIABLE FOR THE LCD SCREEN
lcd = LCD_I2C(39, 16, 2)

# TURN ON THE BACKLIGHT AND CURSOR FOR THE LCD SCREEN
lcd.backlight.on()
lcd.blink.on()

# CREATE AN ENDLESS LOOP THAT GRABS THE TEMP AND HUMIDITY
while True:
    # CAPTURE THE SENSOR TEMP (DEFAULTS TO CELCIUS)
    temp_c = sensor.temperature
    # CAPTURE THE SENSOR HUMIDITY
    humidity = sensor.humidity
    # CONVERT THE TEMP TO FAHRENHEIT
    temp_f = round(temp_c * (9 / 5) + 32)
    # PRINT RESULTS TO CONSOLE
    print(f'\nTemp in C: {temp_c}\nTemp in F: {temp_f}\nHumidity: {humidity}\n____________________')
    # WRITE THE TEMP VALUE TO THE FIRST LINE OF THE LCD
    lcd.cursor.setPos(0,0)
    lcd.write_text(f'Temp={temp_c}C')
    # WRITE THE HUMIDITY VALUE TO THE SECOND LINE OF THE LCD
    lcd.cursor.setPos(1,0)
    lcd.write_text(f'Humidity={humidity}%')

    # WAIT 2 SECONDS TO ALLOW SPACE BETWEEN READINGS
    time.sleep(2)