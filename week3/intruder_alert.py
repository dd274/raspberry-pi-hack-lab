'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This program uses an HC-SR04 Ultrasonic
    distance sensor and a buzzer to create a basic
    intruder alarm.
    
    More details:
    https://duke.is/pi-hack-lab3
'''

# Import require libraries
from gpiozero import DistanceSensor, Buzzer
from time import sleep

# Assign connected GPIO pins
# The Echo pin is GPIO 23
# The Trigger pin is GPIO 24
ultrasonic = DistanceSensor(23, 24)
# The buzzer is connected to GPIO 17
buzzer = Buzzer(17)

# Set the distance threshold for
# when the alarm should be triggered.
ultrasonic.threshold_distance = 0.2

# Create a 'while True' loop that loops indefinietely
while True:
    # Wait for the object to come within the threshold
    ultrasonic.wait_for_in_range()
    # Print the distance
    print('In range: ', ultrasonic.distance, 'm')
    # Turn the buzzer on
    buzzer.on()
    # Wait 1 second
    sleep(1)
    # Turn the buzzer off
    buzzer.off()
    
    # Wait for the object to leave the threshold
    sensor.wait_for_out_of_range()
    # Print the distance
    print('Out of range :', sensor.distance, 'm')