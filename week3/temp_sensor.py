'''
    This code is part of the Raspberry Pi Hack Lab
    Series. In this lab exercise, the primary focus
    is sensors. This is the basice code for a DHT11
    temperature sensor.

    Libraries to install:
    pip install adafruit-circuitpython-dht
    
    More details:
    https://duke.is/pi-hack-lab3

'''

# IMPORT REQUIRED LIBRARIES
import adafruit_dht
import board
import time

# CREATE A VARIABLE FOR THE DHT11 SENSOR
sensor = adafruit_dht.DHT11(board.D4)

# CREATE AN ENDLESS LOOP THAT GRABS THE TEMP AND HUMIDITY
while True:
    # CAPTURE THE SENSOR TEMP (DEFAULTS TO CELCIUS)
    temp_c = sensor.temperature
    # CONVERT THE TEMP TO FAHRENHEIT
    temp_f = round(temp_c * (9 / 5) + 32)
    # CAPTURE THE SENSOR HUMIDITY
    humidity = sensor.humidity
    # PRINT RESULTS TO CONSOLE
    print(f'\nTemp in C: {temp_c}\nTemp in F: {temp_f}\nHumidity: {humidity}\n____________________')
    # WAIT 2 SECONDS TO ALLOW SPACE BETWEEN READINGS
    time.sleep(2)