<h1 align="left">Raspberry Pi Hack Lab 🍓⚙️🤖</h1>

###

<p align="left">The [Raspberry Pi Hack Lab](https://duke.is/pi-hack-lab) is a weekly series of Raspberry Pi projects designed to take you from novice to advanced in 12 weeks. This repository contains the code and wiring diagrams for each week's projects.</p>

###

<h2 align="left">About The "Hack Lab" Series</h2>

###

<p align="left">The "Hack Lab" is a series of instructor led training. Each bi-weekly session covers a different aspect of the Raspberry Pi. Here's a list of what is covered in each session.<br><br>🏁 Week 1 - Introduction to Raspberry Pi.<br>💡Week 2 - Make It Glow<br>🤏🏾 Week 3 - Make It Feel<br>🏃💨 Week 4 - Make It Move<br>👀 Week 5 - Make It See<br>🧠 Week 6 - Make It Think</p>

###

<h2 align="left">About the "Open Hack Labs" Projects</h2>

###

<p align="left">The "Open Hack Lab" projects are bi-weekly projects that are self-paced, but with instructor support. Here's a list of what is covered in each project<br><br>Project 1 - Raspberry Pi Video Player<br>Project 2 - Voice controlled lights<br>Project 3 - Weather Station<br>Project 4 - Bluetooth Controlled Motors<br>Project 5 - Facial Recognition Lock<br>Project 6 - Object Detecting Robot</p>

