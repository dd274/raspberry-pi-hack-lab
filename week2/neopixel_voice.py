'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to control a
    WS2812B LED strip using a microphone and your
    voice. Here are the libraries that need to be 
    installed:

    NOTE: if you are using a python venv, then add 
    --break-system-packages to the end of each
    pip command

    pip install SpeechRecognition
    pip install sounddevice
    pip install PyAudio
    pip install gipozero
    pip install adafruit-circuitpython-neopixel-spi

    You will also need to make sure these programs are installed:
    
    sudo apt-get install -y pulseaudio flac python3-pyaudio libportaudio2

    More details:
    https://duke.is/pi-hack-lab2'''

from time import sleep # for setting a timer for run the code
import board # for sending the data through the GPIO pins
import neopixel_spi # for controlling the pixels through SPI
import speech_recognition as sr
import sounddevice
import re

# Create a variable that captures the speech for recognition
voice = sr.Recognizer()

# Initialize the LED strip
# It is connected to the SPI interface
# There are 10 lights in the LED strip
my_lights = neopixel_spi.NeoPixel_SPI(board.SPI(), 10)

# Set the brightness of the pixels. 
# 0 is no brightness and 1 is full brightness
my_lights.brightness = 0.2

# Variable for RGB light color https://g.co/kgs/cgXRudY
# '0xffffff' is white and '0xff0000' is red
lights_color = 0x32a852 # green
lights_off = 0x000000 # This color is black, but it also turns the lights off

# Create a 'while True' loop to loop the indented code indefinitely
while True:
    # Create a loop that listens for speech from the microphone
    with sr.Microphone() as recording:
        print("Say something...")
        voice.pause_threshold = 0.9
        voice.adjust_for_ambient_noise(recording)# , duration=1)
        audio = voice.listen(recording)

    # If speech is recognized, try to process it using speech recognition
    # Report any errors if they are caught.
    try:
        print("processing...")
        speech_text = voice.recognize_google(audio)
        print("I think you said " + speech_text)
    except sr.UnknownValueError:
        print("Could not understand audio")
    except sr.RequestError as e:
        print("Could not request results; {0}".format(e))

    # Search the speech for the keyword.
    # If the term 'lights on' is detected, turn the lights on.
    if re.search('lights on', speech_text):
        # Print to the console that the lights are being turned on
        print('Lights On')
        # Fill the LED pixels with your color
        my_lights.fill(lights_color)
        my_lights.show()
    # Else, if the term 'lights off' is detected, turn the lights off.
    elif re.search('lights off', speech_text):
        # Print to the console that the lights are being turned off
        print('Lights Off')
        # Fill the LED pixels with black
        my_lights.fill(lights_off)
    # Else, print "No key word detected"
    else:
        print("No key word detected")