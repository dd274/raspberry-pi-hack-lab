'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to light an LED.
    
    More details:
    https://duke.is/pi-hack-lab2'''

# Import required libraries
from gpiozero import RGBLED

# Create an rgb_led variable that
# tells which GPIO pins are assigned.
# In this case it's Red = Pin 14,
# Green = Pin 15, Blue = Pin 23.
rgb_led = RGBLED(14, 15, 23)

# Set the RGB LED color.
# Here is a good color picker
# for RGB colors: https://g.co/kgs/oZ93KCx
# Blue
rgb_led.color = (0, 0, 1)
sleep(1)
# Red
rgb_led.color = (1, 0, 0)
sleep(1)
# Green
rgb_led.color = (0, 1, 0)
sleep(1)
# Yellow
rgb_led.color = (0.99, 0.96, 0.09)