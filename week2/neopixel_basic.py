'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to control a
    WS2812B LED strip. Here is the library that needs
    to be used to run this program:

    pip install adafruit-circuitpython-neopixel-spi
    OR
    pip install adafruit-circuitpython-neopixel-spi --break-system-packages
    
    More details:
    https://duke.is/pi-hack-lab2'''

from time import sleep # for setting a timer for run the code
import board # for sending the data through the GPIO pins
import neopixel_spi # for controlling the pixels through SPI

# Initialize the LED strip
# It is connected to the SPI interface
# There are 10 lights in the LED strip
my_lights = neopixel_spi.NeoPixel_SPI(board.SPI(), 10)

# Set the brightness of the pixels. 
# 0 is no brightness and 1 is full brightness
my_lights.brightness = 0.2

# Variable for RGB light color https://g.co/kgs/cgXRudY
# '0xffffff' is white and '0xff0000' is red
lights_color = 0x32a852 # green
lights_off = 0x000000 # This color is black, but it also turns the lights off

# Create a 'while True' loop to loop the indented code indefinitely
while True:
    # Print to the console that the lights are being turned on
    print('Lights On')
    # Fill the LED pixels with your color
    my_lights.fill(lights_color)
    my_lights.show()
    # Pause the code for 2 seconds
    sleep(2)
    #Print to the console that the lights are being turned off
    print('Lights Off')
    # Fill the LED pixels with black
    my_lights.fill(lights_off)
    # Pause the code for 2 seconds
    sleep(2)