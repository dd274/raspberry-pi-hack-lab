'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to control a
    WS2812B LED strip and make it emulate a light chase.
    Here are the libraries that need to be installed:

    pip install gipozero
    OR (if not in a venv)
    pip install gpiozero --break-system-packages

    pip install adafruit-circuitpython-neopixel-spi
    OR (if not in a venv)
    pip install adafruit-circuitpython-neopixel-spi --break-system-packages
    
    More details:
    https://duke.is/pi-hack-lab2'''

from gpiozero import Button # for controlling the button
from time import sleep # for setting a timer for run the code
import board # for sending the data through the GPIO pins
import neopixel_spi # for controlling the pixels through SPI

# Assign connected GPIO pins
# The Button is connected to GPIO pin 21
my_button = Button(21)

# Initialize the LED strip
# It is connected to the SPI interface
# There are 10 lights in the LED strip
my_lights = neopixel_spi.NeoPixel_SPI(board.SPI(), 10)

# Set the brightness of the pixels. 
# 0 is no brightness and 1 is full brightness
my_lights.brightness = 0.2

# Variable for RGB light color https://g.co/kgs/cgXRudY
# '0xffffff' is white and '0xff0000' is red
lights_color = 0x32a852 # green
lights_off = 0x000000 # This color is black, but it also turns the lights off

# Create a 'while True' loop to loop the indented code indefinitely
while True:
    # Print to the console that the lights are being turned on
    print('Chasing Lights')
    # Create a for loop that loops through each pixel individually
    for i in range(10):
        # Fill the LED pixels with your color
        my_lights[i] = lights_color
        my_lights.show()
        # Pause code for 1 second
        sleep(0.1)
        # Fill the LED pixels with black
        my_lights.fill(lights_off)