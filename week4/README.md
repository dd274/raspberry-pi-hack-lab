## Hack Lab - Week 2 💡🚥🚨

### 🎉 Welcome our illuminating "Make it Glow" Lab in the "Raspberry Pi Hack Labs" series! 🌟 How awesome would it be to push a button and watch a dazzling string of LED lights dance to your custom commands? In this weeks lab, we're not just playing with lights; we're exploring the awesome world of custom control systems. You'll learn to harness the power of a Raspberry Pi to bring inanimate objects to life! Whether you're a budding inventor, a DIY enthusiast, or just curious about the magic of electronics, this lab will turn your ideas into a luminous reality. It's not just a class; it's an adventure into the heart of creativity and technology. So, grab your Raspberry Pi, and let's make things glow! 💡✨🔧


<p align="center"><img src="images/neopixel.gif"/></p>


## Week 2 Projects

In This repository, you'll find all the files for the projects intended to be done in week2 including:

1. 💡 Lighting up an LED
2. 🚦 Lighting up an RGB LED
3. 🚨 Lighting up a Neopixel strip
4. 🗣️ Voice Control

## Wiring Diagrams

### LED Diagram

This wiring diagram is a repeat of the LED wiring diagram from [Week 1](https://gitlab.oit.duke.edu/dd274/raspberry-pi-hack-lab/-/tree/master/week1)

<p align="center"><img src="images/led_bb.png" style="width:200px;transform:rotate(-90deg);"/></p>


### RGB LED Diagram

This wiring diagram is for connecting an RGB LED to the Raspberry Pi


<p align="center"><img src="images/rgb_led_bb.jpg" style="width:700px;"/></p>


### Neopixel Diagram

Finally, here is the wiring diagram for connecting a 10 LED Neopixel strip and a button the Raspberry Pi


<p align="center"><img src="images/led_strip_wiring_button.jpg" style="width:800px;"/></p>
