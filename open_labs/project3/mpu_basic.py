'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. The goal of this program is to collect data
     output from the MPU6050 accelerometer sensor. The
     MPU6050 operates through I2C communications, so I2C
     will need to me enabled on the Raspberry Pi. This is
     a simple program that reads the data and prints it out
     to the console. The MPU6050 python library from github
     is required. Download it to the same directory your code
     is located. You can use this command:

     wget https://raw.githubusercontent.com/nickcoutsos/MPU-6050-Python/refs/heads/master/MPU6050.py
    
    More details:
    https://duke.is/pi-open-lab3
'''

# Import libraries
from MPU6050 import MPU6050
from time import sleep

# Initialize the MPU-6050 sensor with the I2C address 0x68.
sensor = MPU6050(0x68)

# Infinite loop to continuously read data from the sensor.
while True:
    # Retrieve accelerometer data from the sensor.
    accel_data = sensor.get_accel_data()
    # Retrieve gyroscope data from the sensor.
    gyro_data = sensor.get_gyro_data()
    # Retrieve temperature data from the sensor.
    temp = sensor.get_temp()

    # Print accelerometer data, rounding it to two decimal places.
    x_accel = round(accel_data['x'], 2)
    y_accel = round(accel_data['y'], 2)
    z_accel = round(accel_data['z'], 2)
    print(f'Acceleromter Data: x={x_accel}, y={y_accel}, z={z_accel}')

    # Print gyroscope data, rounding it to two decimal places.
    x_gyro = round(gyro_data['x'], 2)
    y_gyro = round(gyro_data['y'], 2)
    z_gyro = round(gyro_data['z'], 2)
    print(f'Gyroscope Data: x={x_gyro}, y={y_gyro}, z={z_gyro}')

    # Print the temperature in Celsius and Fahrenheit.
    c_temp = round(temp)
    f_temp = round((temp * 1.8) + 32)
    print(f'Temperature: {c_temp}C or {f_temp}F')
    print('__________________________________\n')

    # Pause for 0.5 seconds before the next read cycle.
    sleep(0.5)