'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. The goal of this program is use the data
     from the MPU6050 to make a rudimentary flight sim. The
     MPU6050 operates through I2C communications, so I2C
     will need to me enabled on the Raspberry Pi.
     The MPU6050 python library from github is required.
     Download it to the same directory your code
     is located. You can use this command:

     wget https://raw.githubusercontent.com/nickcoutsos/MPU-6050-Python/refs/heads/master/MPU6050.py
     
     Download the sky image to your program directory using this command:
     
     wget https://i.postimg.cc/Z4StkypT/sky.webp
    
    More details:
    https://duke.is/pi-open-lab3
'''
# Import libraries
import math
import time
from MPU6050 import MPU6050
import pygame as py

# Initialize the MPU-6050 sensor with the I2C address 0x68.
sensor = MPU6050(0x68)

# Set up details about the game window
py.display.set_caption("Flight Sim")
screen = py.display.set_mode((gameWidth, gameHeight))

# Set the heith/width of the pygame window
gameHeight = 450
gameWidth = 450

# Load the image
bg = py.image.load('sky.webp')

# Set a variable for the size of the image
bg_rect = bg.get_rect()


# Initialize the pitch and roll variables to 0 
roll=0
pitch=0

# Set the speed at which the image should scroll
# in any direction.
pan_speed = 10

# Set the X and Y location values to be in the
# middle of the image starting out
x = -(bg_rect.width - gameWidth) // 2
y = -(bg_rect.height - gameHeight) // 2

# Create a function that grabs and returns values
# from the accelerometer as angles.
def getMovement():
    # Initialize the movement variable as a dict
    movement = {}

    # Read accelerometer and gyroscope data
    accel_data = sensor.get_accel_data()
    gyro_data = sensor.get_gyro_data()

    # Extract accelerometer values
    accel_x = accel_data['x']
    accel_y = accel_data['y']
    accel_z = accel_data['z']

    # Convert to angles
    pitch=math.atan(accel_y/accel_z)
    roll=math.atan(accel_x/accel_z)

    # Store as degrees, rounded to a whole number
    movement['pitch'] = round(pitch/(2*math.pi)*360)
    movement['roll'] = round(roll/(2*math.pi)*360)
    movement['yaw'] = round(gyro_data['z'])

    # Return values
    return movement

# Get initial accelerometer values to compare against
previous_movement = getMovement()

# Instead of setting the while loop to True,
# set it to a variable that can be switched to false.
running = True

# Set a while loop to our "running" variable.
while running:
    # If there is an event, such as exiting the window,
    # stop the loop and quit pygame.
    for event in py.event.get():
        if event.type == py.QUIT:
            running = False

    # Log the current accelerometer values to    
    current_movement = getMovement()
    # Sleep for a short time to avoid excessive output
    time.sleep(0.5)

    # Compare current movement values to initial values.
    # Depending on whether the values or more or less than
    # the initial value, move the image up, down, left or right.
    if current_movement['roll'] > previous_movement['roll']:
        x += pan_speed  # Pan to the right (move image left)
    if current_movement['roll'] < previous_movement['roll']:
        x -= pan_speed  # Pan to the left (move image right)
    if current_movement['pitch'] > previous_movement['pitch']:
        y += pan_speed  # Pan down (move image up)
    if current_movement['pitch'] < previous_movement['pitch']:
        y -= pan_speed  # Pan up (move image down)
        
    # Set the location of the image in the display
    x = min(0, max(-(bg_rect.width - gameWidth), x))
    y = min(0, max(-(bg_rect.height - gameHeight), y))

    # Refresh the image changes
    screen.fill((0, 0, 0))  # Clear the screen
    screen.blit(bg, (x, y))  # Draw the image at the updated position
    py.display.update()  # Update the display

    

# Quit pygame
py.quit()
