## Open Hack Lab - Project 3 📟⚙️✈️

Raspberry Pi's and sensors are a match made in heaven. There are so many AMAZING applications and interesting data that can be collected from the world around you. One cool little sensor is called an accelerometer and it can basically record movement. Specifically, the [MPU6050](https://www.electronicwings.com/sensors-modules/mpu6050-gyroscope-accelerometer-temperature-sensor-module) can record acceleration, gyro movements (turning/twisting), and temperature. For this project, we will be using an MPU6050 to record movement and use that data to create a very rudimentary flight simulator. So grab an MPU6050 and strap into the Pilot's seat, so we can land this lesson!

[Refer to this project page for more information](https://duke.is/pi-open-lab3)

## Project Parts

Here are the parts you'll need to complete this project:

1. Raspberry Pi & Breadboard
2. Jumper Wires
3. [MPU6050 accelerometer](https://www.amazon.com/HiLetgo-MPU-6050-Accelerometer-Gyroscope-Converter/dp/B078SS8NQV)

## Wiring Diagrams

Here is a wiring diagram for adding connecting the MPU6050 accelerometer to your Raspberry Pi. **PLEASE NOTE:** The MPU6050 communicates through I2C protocols, so that will need to be enabled on your Pi. Please refer to the Edstem lesson for more information.

![](MPU6050_bb.jpg)
