'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. The goal of this program is to collect data
     output from the MPU6050 accelerometer sensor and convert
     it into 'Pitch', 'Roll', and 'Yaw' degrees. The
     MPU6050 operates through I2C communications, so I2C
     will need to me enabled on the Raspberry Pi.The MPU6050 
     python library from github is required. Download it to 
     the same directory your code is located. You can use this
     command:

     wget https://raw.githubusercontent.com/nickcoutsos/MPU-6050-Python/refs/heads/master/MPU6050.py
    
    More details:
    https://duke.is/pi-open-lab3
'''

# Import libraries
import math
import time
from MPU6050 import MPU6050


# Initialize the MPU-6050 sensor with the I2C address 0x68.
mpu = MPU6050(0x68)

# Create a function that reeds the MPU6050 data
# and converts it into angles. 
def getMovement():
    # Initialize the movement variable as a dict
    movement = {}

    # Read accelerometer and gyroscope data
    accel_data = mpu.get_accel_data()
    gyro_data = mpu.get_gyro_data()

    # Extract accelerometer values
    accel_x = accel_data['x']
    accel_y = accel_data['y']
    accel_z = accel_data['z']

    # Convert to angles
    pitch=math.atan(accel_y/accel_z)
    roll=math.atan(accel_x/accel_z)

    #Store as degrees, rounded to a whole number
    movement['pitch'] = round(pitch/(2*math.pi)*360)
    movement['roll'] = round(roll/(2*math.pi)*360)
    movement['yaw'] = round(gyro_data['z'])
    
    return movement

# Infinite loop to continuously read data from the sensor.
while True:
    # Calculate the angles
    movement = getMovement()
    
    # Print the angles
    print(f"Pitch: {movement['pitch']}, Roll: {movement['roll']}, Yaw: {movement['yaw']}")

    # Sleep for a short time to avoid excessive output
    time.sleep(0.1)
