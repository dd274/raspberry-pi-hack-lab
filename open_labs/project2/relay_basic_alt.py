'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. This is a basic program that uses a button and
     a relay to trigger components that have an external power
     source. This alternative method uses a while loop and
     custom functions to trigger the relay.
    
    More details:
    https://duke.is/pi-open-lab2
'''


# Import libraries
from gpiozero import LED, Button
from time import sleep
from signal import pause

# Assign connected GPIO pins
# The Relay is connected to GPIO pin 16
# The Button is connected to GPIO pin 21
my_relay = LED(16)
my_button = Button(21)

# Create a custom function that assigns the
# relay a value of 1 (turning it on)
def relay_on():
     my_relay.value = 1
     print("relay_on")

# Create a custom function that assigns the
# relay a value of 0 (turning it off)
def relay_off():
     my_relay.value = 0
     print("relay off")

# Create a while True loop that checks for the state
# of the button (1 for pressed, 0 for not pressed) and
# calls a function accordingly.    
while True:
     if my_button.value == 1:
          relay_on()
          sleep(0.5)
     else:
          relay_off()
          sleep(0.5)
     sleep(0.5)