'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. This is a basic program that uses a button and
     a relay to trigger components that have an external power
     source.
    
    More details:
    https://duke.is/pi-open-lab2
'''

# Import libraries
from gpiozero import LED, Button
from signal import pause

# Assign connected GPIO pins
# The Relay is connected to GPIO pin 16
# The Button is connected to GPIO pin 21
my_relay = LED(16)
my_button = Button(21)

# When the button is pressed, turn the relay on.
my_button.when_pressed = my_relay.on

# When the button is released, turn the relay off.
my_button.when_released = my_relay.off

# Pause the code until an event (button press) happens
pause()