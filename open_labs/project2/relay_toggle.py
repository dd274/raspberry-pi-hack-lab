'''
     This code is part of the Raspberry Pi Open Hack Lab
     Series. This is a basic program that uses a button and
     a relay to trigger components that have an external power
     source. This alternative code is an attempt to reduce the
     'debouncing' of the button state.
    
    More details:
    https://duke.is/pi-open-lab2
'''

# Import libraries
from gpiozero import LED, Button
from time import sleep

# Assign connected GPIO pins
# The Relay is connected to GPIO pin 16
# The Button is connected to GPIO pin 21
my_relay = LED(16)
# Added "debounce" option to allow for .2 seconds
# of wait time between button clicks.
my_button = Button(21, bounce_time = 0.2)

# Create a while True loop that runs indefinitely
while True:
     # Check the state of the button to see if it is 1 (pressed)
     if my_button.value == 1:

          # If the button is pressed, toggle the state of the relay (1 or 0)
          my_relay.toggle()

          # If the relay state is 1, print "relay on", else print "relay off"
          if my_relay.value == 1:
               print("relay_on")
          elif my_relay.value == 0:
               print("relay off")
     
     # Pausing the code for 0.5 seconds helps with debouncing.
     sleep(0.5)