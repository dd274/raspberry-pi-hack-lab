## Open Hack Lab - Project 2 ⚙️🔋🪭

It's an amazing feeling when you first control something with a Raspberry Pi! LED's, sensors, motors, they're fun for nice little projects, but they're limited to the power the power the Raspberry Pi can provide to it. But what if you wanted to go BIGGER? If you want to control things like lamps, microwaves, TV's, etc, then you'll want to know about relays. Enter relays—the perfect solution to bridge the gap between low-voltage control and high-voltage power. With a relay, your Raspberry Pi can take command of larger devices like large motors, lamps, TV's or home appliances, making automation projects and DIY experiments a breeze. In this lesson, we'll make relays more "relaytable" 😜📺✨

[Refer to this project page for more information](https://duke.is/pi-open-lab2)
## Project Parts

Here are the parts you'll need to complete this project:

1. Raspberry Pi & Breadboard
2. Jumper Wires
3. [5v Relay](https://www.amazon.com/Valefod-1-Channel-Optocoupler-Isolation-Trigger/dp/B07WQH63FBhttps:/)
4. [USB Breakout Board](https://www.amazon.com/Female-Terminal-Adapter-Breakout-Output/dp/B0BK79N1Y8https:/)
5. [USB Fan](https://www.amazon.com/gp/product/B07PWD24LL)
6. [5v Power Adapters](https://www.amazon.com/Arkare-100V-240V-Replacement-Monitor-Scanner-Raspberry/dp/B09W96X88K)

## Wiring Diagrams

Here is a wiring diagram for adding an LCD to the Raspberry Pi's GPIO pins using a GPIO adapter and a breadboard. **Please note:** that not all relays and USB breakout boards are designed the same. Your wiring ports may differ than below. Double-check your wiring to make sure it's going to the right location

![](relay_usb_bb.jpg)
