<h1 align="left">Open Hack Lab - Project 1 🍓🎥</h1>

###

<p align="left">This first Open Hack Lab project is about adding a screen to your project! So in this weeks project, we will be creating a push-button media player that displays information about the media that's being played on our little 16x2 LCD screen!</p>
<center><img src="video_player.gif" alt="Video player example" width="200"/></center>

###

<h2 align="left">Project 1 Parts</h2>

###

<p align="left">In this repository, you will find files for the projects intended to be done in week 1, including:
<br>
<ol>
<li>Raspberry Pi & breadboard
<li>LED, resistor and button [(from Week 1)](https://gitlab.oit.duke.edu/dd274/raspberry-pi-hack-lab/-/tree/master/week1)
<li>16 x 2 LCD screen
<li> Jumper wires
</ol>
</p>

###

<h2 align="left">Wiring Diagrams</h2>

###
<h3>LCD Diagram</h3>

<p align="left">Here is a wiring diagram for adding an LCD to the Raspberry Pi's GPIO pins using a GPIO adapter and a breadboard. This builds on top of the LED and button project from [Week 1](https://gitlab.oit.duke.edu/dd274/raspberry-pi-hack-lab/-/tree/master/week1)</p>

<center><img src="video_player_bb.jpg" alt="LCD Breadboard Diagram" width="200"/></center>
