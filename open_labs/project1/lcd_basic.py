'''
    This code is part of the Raspberry Pi Open Hack Lab
    Series. This program uses a button and a 16x2 LCD
    screen to display text. The LCD requires the
    "rpi-lcd" library to work:
    
    pip install lcd-i2c
    
    More details:
    https://duke.is/pi-open-lab1
'''
# Import necessary libraries
from lcd_i2c import LCD_I2C

# Create a variable for the LCD screen
lcd = LCD_I2C(39, 16, 2)

# Turn on the backlight
lcd.backlight.on()

# Show the blinking cursor
lcd.blink.on()
# or:
# lcd.cursor.blink.on()

# Print some text
lcd.cursor.setPos(0,5)
lcd.write_text('Hello')
lcd.cursor.setPos(1, 1)
lcd.write_text('Raspberry Pi!')