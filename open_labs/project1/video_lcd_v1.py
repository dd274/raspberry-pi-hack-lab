'''
    This code is part of the Raspberry Pi Open Hack Lab
    Series. This program uses a button and a 16x2 LCD
    screen to make a push-button video player that
    shows the video details on the 16x2 LCD screen. The
    rpi-lcd and VLC python libraries are required for
    this program to run:
    
    pip install lcd-i2c
    pip install python-vlc
    
    Demo video file can be downloaded using this command:
    wget https://archive.org/download/big-buck-bunny-640x-360/BigBuckBunny%20640x360.ia.mp4 -O ~/big_buck_bunny.mp4
    
    More details:
    https://duke.is/pi-open-lab1
'''

# Import necessary libraries
import vlc
import time
import os
from lcd_i2c import LCD_I2C
from gpiozero import Button, LED

# Create a variable for the video file
home_folder = os.path.expanduser("~")
video_file = home_folder + "/big_buck_bunny.mp4"
video_name = os.path.basename(video_file)

# Create a variable for the LCD screen
lcd = LCD_I2C(39, 16, 2)

# Turn on the backlight
lcd.backlight.on()

# Show the blinking cursor
lcd.blink.on()

# Assign connected GPIO pins
# The Button is connected to GPIO pin 21
# The LED is connected to GPIO pin 20
my_button = Button(21)
my_led = LED(20)

# Create a variable for the media player
my_video = vlc.MediaPlayer(video_file)

# Create a 'while True' loop that loops
# through this code indefinitely

while True:
    # Wait for the button to be pressed
    my_button.wait_for_press()
    print("Button is pressed")
    
    # Toggle the state of the LED. If it's
    # on (1), turn it off (0), and vice versa.
    my_led.toggle()
    
    # Check the current state of the relay to see
    # if it is on (1) or off (0)
    if my_led.value == 1:
        # Set the video to full screen
        #my_video.toggle_fullscreen()
        # Play the video
        my_video.play()
        # Display the video name to the first
        # line of the LCD screen.
        lcd.cursor.setPos(0,0)
        lcd.write_text(video_name)
        print("video started")
    else:
        # If the state of the button is off (0)
        # then stop the video and clear the LCD.
        my_video.stop()
        lcd.clear()
        print("video stopped")
    
    # Print out the value of the relay
    # for troubleshooting purposes
    print(my_led.value)
    
    # Wait for the button to be released
    # before continuing the code.
    my_button.wait_for_release()
