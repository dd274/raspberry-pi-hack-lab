#!/usr/bin/python3
from flask import Flask, render_template, request
from gpiozero import Robot
from time import sleep

app = Flask(__name__)

robot = Robot(left=(4, 14), right=(18, 17))

@app.route('/')
def index():
    return render_template('robot.html')

@app.route('/up')
def botForward():
    robot.forward()
    sleep(1)
    robot.stop()
    return render_template('robot.html')

@app.route('/down')
def botBack():
    robot.backward()
    sleep(1)
    robot.stop()
    return render_template('robot.html')
    # return jsonify(ledState=0)

@app.route('/left')
def botLeft():
    robot.left()
    sleep(0.25)
    robot.stop()
    return render_template('robot.html')

@app.route('/right')
def botRight():
    robot.right()
    sleep(0.25)
    robot.stop()
    return render_template('robot.html')

@app.route('/stop')
def botStop():
    robot.stop()
    return render_template('robot.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
