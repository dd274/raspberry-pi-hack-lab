# sudo apt install libcamera-dev
# sudo apt install libcap-dev
# python -m venv --system-site-packages cv_venv
# ln -s /usr/lib/python3/dist-packages/v4l2 myenv/lib/python3.9/site-packages/
import cv2
import numpy as np
from picamera2 import Picamera2
from tflite_runtime.interpreter import Interpreter, load_delegate
from pathlib import Path

project_folder = str(Path.home()) + "/raspberry-pi-hack-lab/week6/"

# Load the labels
labels_path = project_folder + "ssd_models/labelmap.txt"
with open(labels_path, 'r') as f:
    labels = [line.strip() for line in f.readlines()]
# The first label of COCO is 'background' which should be removed
labels = labels[1:]

# Initialize the TensorFlow Lite interpreter
model_path = project_folder + "ssd_models/detect.tflite"
interpreter = Interpreter(model_path)#, experimental_delegates=[load_delegate('libedgetpu.so.1')])
interpreter.allocate_tensors()

# Get model input details and resize image function
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
_, height, width, _ = input_details[0]['shape']

# Function to preprocess the image
def preprocess_image(image):
    image = cv2.resize(image, (width, height))
    image = image[:, :, :3]
    image = np.expand_dims(image, axis=0)
    image = np.uint8(image)
    return image

# Initialize Picamera2
picam2 = Picamera2()
preview_config = picam2.create_preview_configuration(main={"format": 'XRGB8888', "size": (640, 480)})
picam2.configure(preview_config)
picam2.start()

try:
    while True:
        # Capture image
        frame = picam2.capture_array()

        # Preprocess the image
        input_data = preprocess_image(frame)

        # Run the model
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()

        # Get detection boxes, classes, and scores
        boxes = interpreter.get_tensor(output_details[0]['index'])[0] # Bounding box coordinates of detected objects
        classes = interpreter.get_tensor(output_details[1]['index'])[0] # Class index of detected objects
        scores = interpreter.get_tensor(output_details[2]['index'])[0] # Confidence of detected objects

        # Loop over all detections and draw bounding box and label if confidence is above a threshold, e.g., 0.5
        for i in range(len(scores)):
            if scores[i] > 0.5: # Confidence threshold
                # Get bounding box coordinates and draw box
                # Interpreter returns box coordinates in the order [ymin, xmin, ymax, xmax]
                ymin, xmin, ymax, xmax = boxes[i]
                xmin = int(xmin * 640)
                xmax = int(xmax * 640)
                ymin = int(ymin * 480)
                ymax = int(ymax * 480)
                cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)

                # Draw label
                object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
                label = f'{object_name}: {int(scores[i]*100)}%'
                cv2.putText(frame, label, (xmin, ymin-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # Display the results
        cv2.imshow('Object Detection', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
finally:
    cv2.destroyAllWindows()
    picam2.stop()

