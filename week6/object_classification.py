import cv2
import numpy as np
from picamera2 import Picamera2
from tflite_runtime.interpreter import Interpreter
from pathlib import Path

project_folder = str(Path.home()) + "/raspberry-pi-hack-lab/week6/"

# Initialize the TensorFlow Lite interpreter
model_path = project_folder + "models/mobilenet_v1_1.0_224_quant.tflite" 
interpreter = Interpreter(model_path)
interpreter.allocate_tensors()

# Get model input details and resize image function
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
height = input_details[0]['shape'][1]
width = input_details[0]['shape'][2]

# Load the labels
labels_path = project_folder + "models/labels_mobilenet_quant_v1_224.txt"
with open(labels_path, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

# Initialize Picamera2
picam = Picamera2()
picam.preview_configuration.main.size = (640, 480)
picam.preview_configuration.main.format = "RGB888"
picam.preview_configuration.align()
picam.configure("preview")
picam.start()

while True:
    # Capture image
    img = picam.capture_array()
    
    # Preprocess the image
    #input_data = preprocess_image(img)
    #input_data = np.expand_dims(cv2.resize(img[:, :, :3], (width, height)), axis=0).astype(np.uint8)
    resize_img = cv2.resize(img, (width, height))
    rgb_img = resize_img[:, :, :3]
    expand_img = np.expand_dims(rgb_img, axis=0)
    converted_img = np.uint8(expand_img)
    
    # Run the model
    interpreter.set_tensor(input_details[0]['index'], converted_img)
    interpreter.invoke()
    
    # Get detection results
    output_data = interpreter.get_tensor(output_details[0]['index'])
    results = np.squeeze(output_data)
    
    # Find the best prediction
    top_prediction = results.argsort()[-1]
    print(results[top_prediction])
    confidence = results[top_prediction]
    label = labels[top_prediction]
    
    # Display the results
    cv2.putText(img, f"{label}: {confidence:.2f} confidence", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.imshow('Object Classification', img)
    
    if cv2.waitKey(10) == ord('q'):
        picam.stop()
        cv2.destroyAllWindows()
        break
