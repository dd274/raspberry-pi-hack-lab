'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to blink an LED
    on and off.    
    
    More details:
    https://duke.is/pi-hack-lab1
'''

# Import necessary libraries
from gpiozero import LED, Button
import time

# Assign connected GPIO pins
# The Button is connected to GPIO pin 21
# The LED is connected to GPIO pin 20
my_button = Button("GPIO21")
my_led = LED("GPIO20")

# Create a 'while True' loop that loops forever
while True:
    # When the button is pressed, turn the LED on
    my_button.when_pressed = my_led.on

    # When the button is released, turn the LED off
    my_button.when_released = my_led.off