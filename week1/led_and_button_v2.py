'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to blink an LED
    on and off.    
    
    More details:
    https://duke.is/pi-hack-lab1
'''

# Import necessary libraries
from gpiozero import LED, Button
import time

# Assign connected GPIO pins
# The Button is connected to GPIO pin 21
# The LED is connected to GPIO pin 20
my_button = Button("GPIO21")
my_led = LED("GPIO20")

# Create a 'while True' loop that loops forever
while True:
    # Wait for the button to be pressed.
    my_button.wait_for_press()
    # When pressed, toggle the state of
    # the LED from 1 to 0 (on to off) or
    # from 0 to 1 (off to on).
    my_led.toggle()
    # Wait for the button to be released.
    my_button.wait_for_release()