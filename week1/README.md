## [Hack Lab - Week 1](https://gitlab.oit.duke.edu/dd274/raspberry-pi-hack-lab/-/tree/master/week1) 🍓🏁⚙️

#### ⚙️💡Ever wanted a tool that could take all of your crazy and creative ideas and make them a reality? Then welcome the electrifying world of "Raspberry Pi Hack Labs"! 🍓 Whether you're itching to play with sensors, control dazzling light displays, explore the realm of computer vision, rev up motors, or even dabble in AI – this is your playground! Each "Hack Lab" in this series is a new adventure, where we will explore the amazing plethora of things a Raspberry Pi can do! Stick with it and creating AI powered robots will be a piece of cake! Anything's possible when you're a Raspberry Pi wizard! 🧙‍♂️ So, gear up for a journey where coding meets creativity, and let's hack the ordinary into the extraordinary! 🌟🤖
<div align="center">
<p align="center"><img src="images/blinking.gif"/></p>
</div>

## Week 1 Projects

In This repository, you'll find all the files for the projects intended to be done in week2 including:

1. 🚨 Make an LED Blink
2. 🔘 Light up an LED with a button
3. 🔘 Toggle an LED with a button

## Wiring Diagrams

### LED Diagram

Here is the wiring diagram for connecting an LED and resistor the Raspberry Pi

<p align="center"><img src="images/led_bb.png" style="width:200px;transform:rotate(-90deg);"/></p>

### LED and Button Diagram

This wiring diagram is for connecting an LED and a button to the Raspberry Pi

<p align="center"><img src="images/button_led_bb.png" style="width:200px;transform:rotate(-90deg);"/></p>
