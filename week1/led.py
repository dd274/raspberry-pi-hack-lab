'''
    This code is part of the Raspberry Pi Hack Lab
    Series. This is a basic program to blink an LED
    on and off.    
    
    More details:
    https://duke.is/pi-hack-lab1
'''

# Import necessary libraries
from gpiozero import LED
import time

# Assign connected GPIO pins
# The LED is connected to GPIO pin 20
my_led = LED("GPIO20")

# Create a 'while True' loop that loops forever
while True:
    # Then turn the LED on.
    my_led.on()
    # Wait for 1 second.
    time.sleep(1)
    # Then turn the LED off.
    my_led.off()
    # Wait for 1 second.
    time.sleep(1)